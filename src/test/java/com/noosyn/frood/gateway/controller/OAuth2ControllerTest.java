package com.noosyn.frood.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.noosyn.frood.gateway.model.ApiError;
import com.noosyn.frood.gateway.model.Token;
import com.noosyn.frood.gateway.util.TestDataMaker;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@WebMvcTest(value = OAuth2Controller.class, secure = false)
public class OAuth2ControllerTest {
	@MockBean
	private RestTemplate restTemplate;
	@InjectMocks
	@Spy
	private OAuth2Controller controller;
	@Autowired
	private MockMvc mvc;

	@Test
	public void shouldReturnAccessToken_with_correct_credentials() throws Exception {
		String url = String.format(TestDataMaker.OAUTH_URI, "kuldeep.yadav@noosyntech.in", "123@456");

		when(restTemplate.exchange(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.<HttpMethod>any(),
				ArgumentMatchers.any(),
				ArgumentMatchers.<Class<Token>>any()
		)).thenReturn(new ResponseEntity<Token>(TestDataMaker.createDummyAccessToken(), HttpStatus.OK));

		mvc.perform(MockMvcRequestBuilders.post(url)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.access_token", Matchers.notNullValue()));
		verify(restTemplate, times(1)).exchange(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.<HttpMethod>any(),
				ArgumentMatchers.any(),
				ArgumentMatchers.<Class<Token>>any()
		);
		verifyNoMoreInteractions(restTemplate);
	}

	@Test
	public void shouldReturn_401_status_with_message_for_wrong_credentials() throws Exception {
		String url = String.format(TestDataMaker.OAUTH_URI, "", "123@456");

		String errorMessage = "Invalid Credentials";
		ApiError error = new ApiError();
		error.setErrorMsg(errorMessage);

		when(restTemplate.exchange(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.<HttpMethod>any(),
				ArgumentMatchers.any(),
				ArgumentMatchers.<Class<Token>>any()
		)).thenThrow(new RestClientResponseException(
				"Invalid Credentials", HttpStatus.UNAUTHORIZED.value(),
				HttpStatus.UNAUTHORIZED.toString(),
				new HttpHeaders(),
				new ObjectMapper().writeValueAsBytes(error),
				Charset.defaultCharset()
		));

		mvc.perform(MockMvcRequestBuilders.post(url)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized());
		verify(restTemplate, times(1)).exchange(
				ArgumentMatchers.anyString(),
				ArgumentMatchers.<HttpMethod>any(),
				ArgumentMatchers.any(),
				ArgumentMatchers.<Class<Token>>any()
		);
		verifyNoMoreInteractions(restTemplate);
	}
}
