package com.noosyn.frood.gateway.controller;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.noosyn.frood.gateway.FroodGatewayApplication;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FroodGatewayApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class BaseControllerIT {
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8081);
    @Autowired
    protected MockMvc mvc;
}
