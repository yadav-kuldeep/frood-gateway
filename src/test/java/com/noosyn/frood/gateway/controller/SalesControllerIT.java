package com.noosyn.frood.gateway.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.noosyn.frood.gateway.model.ApiUri;
import com.noosyn.frood.gateway.util.TestDataMaker;
import org.eclipse.jetty.http.HttpHeader;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.noosyn.frood.gateway.util.TestDataMaker.*;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SalesControllerIT extends BaseControllerIT{
    private String screenCode = "ODR002";
    @Test
    public void should_load_SalesOrders_with_no_params() throws Exception {

        stubFor(get(String.format(ApiUri.COLUMNS, screenCode))
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                        .willReturn(
                          aResponse()
                          .withStatus(HttpStatus.OK.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createColumns())))
                        );

        stubFor(get(ApiUri.OPERATORS)
                .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                .willReturn(
                        aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createOperators())))
        );


        stubFor(get(String.format(ApiUri.FILTERS, screenCode))
                .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                .willReturn(
                        aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createFilters())))
        );

        mvc.perform(MockMvcRequestBuilders.get(SALES_URI)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
                    .header(AUTHORIZATION_HEADER, ACCESS_TOKEN)
                    )
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalRows", Matchers.greaterThan(0)))
                .andExpect(jsonPath("$.columns", notNullValue()))
                .andExpect(jsonPath("$.filters", notNullValue()))
                .andExpect(jsonPath("$.operators", notNullValue()));

    }

    @Test
    public void should_fail_with_one_of_endpoints_fails() throws Exception {
        stubFor(get(String.format(ApiUri.COLUMNS, screenCode))
                .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                .willReturn(
                        aResponse()
                                .withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createDummyApiError())))
        );

        stubFor(get(ApiUri.OPERATORS)
                .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                .willReturn(
                        aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createOperators())))
        );


        stubFor(get(String.format(ApiUri.FILTERS, screenCode))
                .withHeader(HttpHeaders.AUTHORIZATION, equalTo(ACCESS_TOKEN))
                .willReturn(
                        aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withHeader(HttpHeader.CONTENT_TYPE.asString(), MediaType.APPLICATION_JSON_VALUE)
                                .withBody(new ObjectMapper().writeValueAsBytes(TestDataMaker.createFilters())))
        );

        mvc.perform(MockMvcRequestBuilders.get(SALES_URI)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .header(AUTHORIZATION_HEADER, ACCESS_TOKEN)
        )
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.errorMsg", Matchers.notNullValue()));
    }

}
