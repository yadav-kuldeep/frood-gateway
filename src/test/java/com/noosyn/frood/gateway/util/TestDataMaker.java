package com.noosyn.frood.gateway.util;

import com.noosyn.frood.gateway.model.*;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class TestDataMaker {
	private TestDataMaker() {
		
	}
	
	public static final String OAUTH_URI = "/api/v1/oauth/token?username=%s&password=%s";
	public static final String SALES_URI = "/api/v1/sales";

	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String ACCESS_TOKEN = "Bearer eyJhbGciOiJSUzI1NiJ9.eyJjb21wYW55SWQiOiJGcnVpdHNDbyIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJjdXN0b21pemF0aW9uSW5mbyI6IntcInpvbmVJZFwiOlwiQXNpYS9TaW5nYXBvcmVcIiwgXCJkYXRlVGltZUZvcm1hdFwiOlwiZGQvTU0veXl5eSBoOm1tIGFcIiwgXCJkYXRlRm9ybWF0XCI6XCJkZC9NTS95eXl5XCJ9IiwiYXBwbGljYXRpb25Db2RlIjoiRlJPT0RFUlAiLCJ1c2VySWQiOiIxIiwiaWF0IjoxNTMwNzk2MjUxLCJhdXRob3JpdGllcyI6WyJWSUVXX0VWRU5UIiwiVklFV19FVkVOVF9BQ1RJT05fTUFQUElORyIsIkRFTEVURV9FVkVOVF9BQ1RJT05fTUFQUElORyIsIlVQREFURV9FVkVOVF9BQ1RJT05fTUFQUElORyIsIlNBVkVfRVZFTlRfQUNUSU9OX01BUFBJTkciLCJDUkVBVEVfRVZFTlQiXX0.VUo_GfG817zOsK_pqOaTbIgoZzvCm1nQlFHAW7L-nvFTNtZv3hENp9vhN_h_mmWZZKmgchHDxotjnYUPYTC50bDRVEZSygrSsjLmo7jkibukXO5Oeymbb44QoNsY48uVNPFlf3NGn7bOkhs4MJLANH3fIynoTKlvHjrbK0w-S9joq4BkwhYrtujo_n5AhLxwitVKDCmJWAiB1aQkPgxiOxMIMzxp2WydUfk8ul_ACOPBLAK2YQyOPW6dpJ9m41RhkT5Z6TTupQmuAVkGyVdJk0H3bWaC26FIvH3ZQnwulZFm0fA8E8mCjHgqCVDHTfV_3zVXDd8p-YaGlFalgVYwdQ";

	public static Token createDummyAccessToken() {
		Token t = new Token();
		t.setAccessToken("eyJhbGciOiJSUzI1NiJ9.eyJjb21wYW55SWQiOiJGcnVpdHNDbyIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJjdXN0b21pemF0aW9uSW5mbyI6IntcInpvbmVJZFwiOlwiQXNpYS9TaW5nYXBvcmVcIiwgXCJkYXRlVGltZUZvcm1hdFwiOlwiZGQvTU0veXl5eSBoOm1tIGFcIiwgXCJkYXRlRm9ybWF0XCI6XCJkZC9NTS95eXl5XCJ9IiwiYXBwbGljYXRpb25Db2RlIjoiRlJPT0RFUlAiLCJ1c2VySWQiOiIxIiwiaWF0IjoxNTMwNzk2MjUxLCJhdXRob3JpdGllcyI6WyJWSUVXX0VWRU5UIiwiVklFV19FVkVOVF9BQ1RJT05fTUFQUElORyIsIkRFTEVURV9FVkVOVF9BQ1RJT05fTUFQUElORyIsIlVQREFURV9FVkVOVF9BQ1RJT05fTUFQUElORyIsIlNBVkVfRVZFTlRfQUNUSU9OX01BUFBJTkciLCJDUkVBVEVfRVZFTlQiXX0.VUo_GfG817zOsK_pqOaTbIgoZzvCm1nQlFHAW7L-nvFTNtZv3hENp9vhN_h_mmWZZKmgchHDxotjnYUPYTC50bDRVEZSygrSsjLmo7jkibukXO5Oeymbb44QoNsY48uVNPFlf3NGn7bOkhs4MJLANH3fIynoTKlvHjrbK0w-S9joq4BkwhYrtujo_n5AhLxwitVKDCmJWAiB1aQkPgxiOxMIMzxp2WydUfk8ul_ACOPBLAK2YQyOPW6dpJ9m41RhkT5Z6TTupQmuAVkGyVdJk0H3bWaC26FIvH3ZQnwulZFm0fA8E8mCjHgqCVDHTfV_3zVXDd8p-YaGlFalgVYwdQ");
		return t;
	}

    public static ApiError createApiErrorForInvalidCredentials() {
		ApiError error = new ApiError();
		error.setErrorMsg("Invalid Credentials");
		return error;
    }

    public static List<Column> createColumns() {
		List<Column> columns = new ArrayList<>(1);
		Column column = new Column();
		column.setId(1);
		column.setDataAlias("orderNo");
		column.setIsDefault(true);
		column.setIsFilter(true);
		column.setName("Order#");
		column.setOrder(0);
		column.setTableDataType("string");
		columns.add(column);
		return columns;
    }

    public static List<Operator> createOperators(){
		List<Operator> operators = new ArrayList<>(1);
		Operator operator = new Operator();
		operator.setId(1);
		operator.setDescription("Is greater than equal to");
		operator.setOperator(">=");
		operator.setType("");
		operators.add(operator);
		return operators;
	}

    public static List<Filter> createFilters(){
		List<Filter> filters = new ArrayList<>(1);
		Filter filter = new Filter();
		filter.setId(1);
		filter.setName("My Filter");
		filter.setUserDefined(true);
		filter.setValue(createFilterValues());
		filters.add(filter);
		return filters;
	}

	public static List<FilterValue> createFilterValues(){
    	List<FilterValue> filterValues = new ArrayList<>(1);
    	FilterValue value = new FilterValue();
    	value.setColumnId(1);
    	value.setLogicalOpId(1);
    	value.setRelationalOpId(1);
    	value.setValue("FRSC-2018-00012");
    	filterValues.add(value);
    	return filterValues;
	}

	public static ApiError createDummyApiError() {
		ApiError error = new ApiError();
		error.setErrorMsg("Some test error message");
		return error;
	}
}
