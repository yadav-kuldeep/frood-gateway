package com.noosyn.frood.gateway.util;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UrlCreatorTest {
    private String baseUrl = "http://localhost:8081/api";
    @Test
    public void shouldReturnUrlWithUri(){
        assertNotNull(UrlCreator.build(baseUrl, "/test"));
    }

    @Test
    public void shouldReturnUrl_with_null_uri(){
        assertNotNull(UrlCreator.build(baseUrl, null));
    }

    @Test
    public void shouldReturnUrl_with_uri_with_params(){
        String url = "http://localhost:8081/api/test?query=test";
        String uri = "/test?query=%s";
        assertEquals(url, UrlCreator.build(baseUrl, uri, "test"));
    }

    @Test
    public void shouldReturnUrl_with_uri_with_map_params(){
        String url = "http://localhost:8081/api/test?query=test";
        String uri = "/test";
        Map<String, Object> params = new HashMap<>(1);
        params.put("query", "test");
        assertEquals(url, UrlCreator.build(baseUrl, uri, params));
    }

}
