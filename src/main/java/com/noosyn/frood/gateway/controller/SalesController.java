package com.noosyn.frood.gateway.controller;

import com.noosyn.frood.gateway.model.*;
import com.noosyn.frood.gateway.util.UrlCreator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class SalesController extends BaseController {
    @Autowired
    private AsyncRestTemplate restTemplate;

    @GetMapping(value = "/sales", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Fetch all sales orders", notes = "Fetch all sales orders")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched access_token"),
            @ApiResponse(code = 400, message = "Bad Request")})
    public ResponseEntity<GridModel> getSales(@RequestParam(value = "id",required = false) String id, @RequestParam(value="limit",required = false) Integer limit, @RequestParam(value = "offset", required = false) Integer offset,
                             @RequestParam(value = "sortBy",required = false) Integer sortBy, @RequestParam(value = "keyword",required = false) String keyword, @RequestParam(value="sortOrder",required = false) String sortOrder,
                             @RequestParam(value="showAllMatchingResults", required = false) boolean showAllMatchingResults) throws ExecutionException, InterruptedException {
        String query = getQueryString();

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeader());
        HttpEntity<String> entity = new HttpEntity<>("", headers);

        ListenableFuture<ResponseEntity<List<Column>>> columnsResp = restTemplate.exchange(
                UrlCreator.build(baseUrl, new StringBuilder("/meta?screen=ODR002").toString()),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Column>>() {}
        );

        ListenableFuture<ResponseEntity<List<Operator>>> operatorsResp = restTemplate.exchange(
                UrlCreator.build(baseUrl, new StringBuilder("/operators").toString()),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Operator>>() {}
        );

        ListenableFuture<ResponseEntity<List<Filter>>> filtersResp = restTemplate.exchange(
                UrlCreator.build(baseUrl, new StringBuilder("/filters?screenCode=ODR002").toString()),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Filter>>() {}
        );

        ListenableFuture<ResponseEntity<Table<Map<String, Object>>>> ordersResp = restTemplate.exchange(
                UrlCreator.build(baseUrl, new StringBuilder("/order").append("?").append(query).toString()),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<Table<Map<String, Object>>>() {}
        );

        GridModel<String> resp = new GridModel<>();
        resp.setColumns(columnsResp.get().getBody());
        resp.setOperators(operatorsResp.get().getBody());
        resp.setFilters(filtersResp.get().getBody());
        Table orders = ordersResp.get().getBody();
        resp.setTotalRows(orders.getTotalRows());
        resp.setTableData(orders.getTableData());
        return new ResponseEntity<GridModel>(resp, HttpStatus.OK);
    }
}
