package com.noosyn.frood.gateway.controller;

import com.noosyn.frood.gateway.model.Token;
import com.noosyn.frood.gateway.util.UrlCreator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OAuth2Controller extends BaseController {
	@Autowired
	private RestTemplate restTemplate;
	
	@PostMapping(path = "/oauth/token", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Fetch access token", notes = "Fetch access token")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched access_token"),
			@ApiResponse(code = 401, message = "Invalid credentials")})
	public ResponseEntity getOAuth2Token(@RequestParam(required = true) String username, @RequestParam(required= true) String password) {
		return restTemplate.
				exchange(
						UrlCreator.build(baseUrl, new StringBuilder(getRequestUri()).append("?").
								append(getQueryString()).toString()),
						HttpMethod.POST, null, Token.class);
	}

}
