package com.noosyn.frood.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/api/v1")
public class BaseController {
    @Autowired
    private HttpServletRequest request;

    @Value("${erp.service.baseUrl}")
    protected String baseUrl;

    public String getAuthorizationHeader(){
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    public String getRequestUri(){
        return request.getRequestURI().substring(7);
    }

    public String getQueryString(){
        return request.getQueryString();
    }
}
