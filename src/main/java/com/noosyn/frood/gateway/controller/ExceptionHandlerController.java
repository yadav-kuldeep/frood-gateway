package com.noosyn.frood.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.noosyn.frood.gateway.model.ApiError;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
@RestController
@CommonsLog
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    @ExceptionHandler(RestClientResponseException.class)
    public ResponseEntity<ApiError> handleRestClientResponseException(RestClientResponseException ex, WebRequest req) throws IOException {
        String responseBody = ex.getResponseBodyAsString();
        log.debug(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        ApiError error = mapper.readValue(responseBody, ApiError.class);
        return new ResponseEntity<ApiError>(error, HttpStatus.valueOf(ex.getRawStatusCode()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleGenericException(Exception ex, WebRequest req) throws IOException {
        log.error(ex);
        ApiError error = new ApiError();
        error.setErrorMsg(ex.getMessage());
        return new ResponseEntity<ApiError>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
