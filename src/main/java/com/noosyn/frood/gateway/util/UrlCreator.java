package com.noosyn.frood.gateway.util;

import java.util.Map;

public class UrlCreator {
    private UrlCreator(){}

    public static String build(String baseUrl, String uri, Object...params){
        return new StringBuilder(baseUrl)
                .append(String.format(uri, params)).toString();
    }

    public static String build(String baseUrl, String uri){
        return new StringBuilder(baseUrl)
                .append(uri).toString();
    }

    public static String build(String baseUrl, String uri, Map<String, Object> params){
        StringBuilder sb = new StringBuilder(baseUrl).append(uri).append("?");
        for(Map.Entry<String, Object> e: params.entrySet()){
            sb.append(e.getKey()).append("=").append(e.getValue());
        }
        return sb.toString();
    }
}
