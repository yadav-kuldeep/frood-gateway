package com.noosyn.frood.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class FroodGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(FroodGatewayApplication.class, args);
	}
}
