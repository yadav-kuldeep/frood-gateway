package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Table<T> {
    private List<T> tableData;
    private Integer totalRows;
}
