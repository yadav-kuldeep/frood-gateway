package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterValue {
    private Integer columnId;
    private Integer logicalOpId;
    private Integer relationalOpId;
    private String value;
}
