package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Operator {
    private String description;
    private Integer id;
    private String operator;
    private String type;
}
