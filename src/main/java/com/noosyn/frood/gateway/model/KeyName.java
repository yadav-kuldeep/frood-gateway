package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyName {
    private Integer id;
    private String name;
}
