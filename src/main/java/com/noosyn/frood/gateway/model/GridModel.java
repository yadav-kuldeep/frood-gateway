package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GridModel<T> extends Table {
    private List<Column> columns;
    private List<Filter> filters;
    private List<Operator> operators;
}
