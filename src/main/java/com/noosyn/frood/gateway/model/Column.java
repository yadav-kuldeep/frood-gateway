package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Column {
    private String tableDataType;
    private Integer order;
    private String name;
    private String dataAlias;
    private Integer id;
    private Boolean isDefault = false;
    private Boolean isEditable = false;
    private Boolean isFilter = false;
    private Boolean isSortable = false;
    private List<KeyName> availableValues;
}
