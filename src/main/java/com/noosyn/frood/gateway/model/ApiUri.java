package com.noosyn.frood.gateway.model;

public class ApiUri {
    private ApiUri() {

    }

    public static final String COLUMNS = "/PayrollWeb/api/meta?screen=%s";
    public static final String OPERATORS = "/PayrollWeb/api/operators";
    public static final String FILTERS = "/PayrollWeb/api/filters?screenCode=%s";
}
