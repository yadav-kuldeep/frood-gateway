package com.noosyn.frood.gateway.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Filter {
    private Integer id;
    private String name;
    private Boolean userDefined = true;
    private List<FilterValue> value;
}
